val maxRateOfClimb = 5000
val amount = 0.5f
val rateOfClimb = amount.min(1.0f).max(-1.0f) *  maxRateOfClimb

val xs = List(1, 2, 3)
xs.map((42, _ ))

class Helper {
  val answer = 42
  def getAnnswer = answer
}

new Helper {
  println(2 * getAnnswer)
  println(answer)
}