package zzz.akka.avionics

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef}

import scala.concurrent.duration._
import akka.util.Timeout

import scala.concurrent.Await
import scala.concurrent.duration.Duration

trait PilotProvider {
  def newPilot(plane: ActorRef,
               autopilot: ActorRef,
               controls: ActorRef,
               altimeter: ActorRef): Actor = new Pilot(plane, autopilot, controls, altimeter)
  def newCopilot(plane: ActorRef,
                 autopilot: ActorRef,
                 altimeter: ActorRef): Actor = new Copilot(plane, autopilot, altimeter)
  def newAutopilot: Actor = new Autopilot
}

object Pilots {
  case object ReadyToGo
  case object RelinquishControl
}

class Pilot(plane: ActorRef,
            autopilot: ActorRef,
            var controls: ActorRef,
            altimeter: ActorRef) extends Actor {
  import Pilots._
  import Plane._
//  var controls: ActorRef = context.system.deadLetters
  var copilot: ActorRef = context.system.deadLetters
//  var autopilot: ActorRef = context.system.deadLetters
  val copilotName = context.system.settings.config.getString(
    "zzz.akka.avionics.flightcrew.copilotName")
  val atMostDuration = Duration(1000, MILLISECONDS)
  implicit val timeout: Timeout = atMostDuration
  def receive = {
    case ReadyToGo =>
      context.parent ! GiveMeControl
      copilot = Await.result(context.actorSelection("../" + copilotName).resolveOne(), atMostDuration )
//      autopilot = Await.result(context.actorSelection("../Autopilot").resolveOne(), atMostDuration)
    case Controls(controlSurfaces) =>
      controls = controlSurfaces
  }
}


class Copilot(plane: ActorRef,
              autopilot: ActorRef,
              altimeter: ActorRef) extends Actor {
  import Pilots._
  var controls: ActorRef = context.system.deadLetters
  var pilot: ActorRef = context.system.deadLetters
//  var autopilot: ActorRef = context.system.deadLetters
  val pilotName = context.system.settings.config.getString(
    "zzz.akka.avionics.flightcrew.pilotName")
  val atMostDuration = Duration(1000, MILLISECONDS)
  implicit val timeout: Timeout = atMostDuration

  def receive = {
    case ReadyToGo =>
      pilot = Await.result(context.actorSelection("../" + pilotName).resolveOne(), atMostDuration )
//      autopilot = Await.result(context.actorSelection("../Autopilot").resolveOne(), atMostDuration)
  }
}

class Autopilot extends Actor {
  override def receive: Receive = Actor.emptyBehavior
}