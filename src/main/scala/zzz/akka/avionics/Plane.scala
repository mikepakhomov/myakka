package zzz.akka.avionics

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.duration._
import zzz.akka.avionics.IsolatedLifeCycleSupervisor.WaitForStart

import scala.concurrent.Await

object Plane {
  // Returns the control surface to the Actor that asks for them
  case object GiveMeControl
  // How we respond to the GiveMeControl message
  case class Controls(controls: ActorRef)

  def apply() = new Plane with AltimeterProvider with PilotProvider with LeadFlightAttendantProvider
}

// We want the Plane to own the Altimeter and we're going to
// do that by passing in a specific factory we can use to
// build the Altimeter
class Plane extends Actor with ActorLogging {
  this: AltimeterProvider
    with PilotProvider
    with LeadFlightAttendantProvider =>

  import Altimeter._
  import Plane._
  import EventSource._

  val cfgstr = "zzz.akka.avionics.flightcrew"
  val altimeter = context.actorOf(
    Props(Altimeter()), "Altimeter")
  val controls = context.actorOf(
    Props(new ControlSurfaces(altimeter)), "ControlSurfaces")
  val config = context.system.settings.config
  val pilotName = config.getString(s"$cfgstr.pilotName")
  val copilotName = config.getString(s"$cfgstr.copilotName")
  val autopilot = context.actorOf(
    Props[Autopilot], "Autopilot")
  val attendantName = config.getString(s"$cfgstr.leadAttendantName")

  // There's going to be a couple of asks below and
  // a timeout is necessary for that.
  implicit val askTimeout = Timeout(1.second)
  def startEquipment() {
    val controls = context.actorOf(
      Props(new IsolatedResumeSupervisor
        with OneForOneStrategyFactory {
        def childStarter() {
          val alt = context.actorOf(
            Props(newAltimeter), "Altimeter")
          // These children get implicitly added to the
          // hierarchy
          context.actorOf(Props(newAutopilot), "Autopilot")
          context.actorOf(Props(new ControlSurfaces(alt)),
            "ControlSurfaces")
        }
      }), "Equipment")
    Await.result(controls ? WaitForStart, 1.second)
  }

  // Helps us look up Actors within the "Equipment" Supervisor
  def actorForControls(name: String) = {
    Await.result(context.actorSelection("Equipment/" + name).resolveOne(), 1.second)
  }
  // Helps us look up Actors within the "Pilots" Supervisor
  def actorForPilots(name: String) = {
    Await.result(context.actorSelection("Pilots/" + name).resolveOne(), 1.second)
  }


  def startPeople() {
    val plane = self
    // Note how we depend on the Actor structure beneath
    // us here by using actorFor().  This should be
    // resilient to change, since we'll probably be the
    // ones making the changes
    val controls = actorForControls("ControlSurfaces")
    val autopilot = actorForControls("Autopilot")
    val altimeter = actorForControls("Altimeter")
    val people = context.actorOf(
      Props(new IsolatedStopSupervisor
        with OneForOneStrategyFactory {
        def childStarter() {
          // These children get implicitly added
          // to the hierarchy
          context.actorOf(
            Props(newCopilot(plane, autopilot, altimeter)),
            copilotName)
          context.actorOf(
            Props(newPilot(plane, autopilot,
              controls, altimeter)),
            pilotName)
        }
      }), "Pilots")
    // Use the default strategy here, which
    // restarts indefinitely
    context.actorOf(Props(newLeadFlightAttendant), attendantName)
    Await.result(people ? WaitForStart, 1.second)
  }


  def receive = {
    case GiveMeControl =>
      log info("Plane giving control.")
      sender ! Controls(controls)
    case AltitudeUpdate(altitude) =>
      log info(s"Altitude is now: $altitude")
  }

  override def preStart() {
    import EventSource.RegisterListener
    import Pilots.ReadyToGo
    // Get our children going.  Order is important here.
    startEquipment()
    startPeople()
    // Bootstrap the system
    actorForControls("Altimeter") ! RegisterListener(self)
    actorForPilots(pilotName) ! ReadyToGo
    actorForPilots(copilotName) ! ReadyToGo
  }
}