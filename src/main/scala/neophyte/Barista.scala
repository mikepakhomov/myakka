package neophyte

import akka.actor.SupervisorStrategy.{Directive, Resume}
import akka.actor.{Actor, OneForOneStrategy, Props, SupervisorStrategy}

object Barista {
  case object EspressoRequest
  case object ClosingTime
  case class EspressoCup(state: EspressoCup.State)
  object EspressoCup {
    sealed trait State
    case object Clean extends State
    case object Filled extends State
    case object Dirty extends State
  }
  case class Receipt(amount: Int)
}

class Barista extends Actor {
  import Barista._
  import EspressoCup._
  import Register._
  import akka.pattern.{ask, pipe}
  import akka.util.Timeout
  import context.dispatcher

  import concurrent.duration._

  implicit val timeout = Timeout(4.seconds)
  val register = context.actorOf(Props[Register], "Register")

  val decider: PartialFunction[Throwable, Directive] = {
    case _: PaperJamException => Resume
  }

//  override def supervisorStrategy: SupervisorStrategy =
//    OneForOneStrategy()(decider.orElse(SupervisorStrategy.defaultStrategy.decider))

  def receive = {
    case EspressoRequest =>
      val receipt = register ? Transaction(Espresso)
      receipt.map(rcpt => (EspressoCup(Filled), rcpt)).pipeTo(sender)
      // MP: the same as above but much shorter
    //receipt.map((EspressoCup(Filled), _)).pipeTo(sender)
    case ClosingTime =>
//      context.system.terminate()
    context.stop(self)
  }
}
