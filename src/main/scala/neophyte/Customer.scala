package neophyte

import akka.actor.{Actor, ActorLogging, ActorRef, Terminated}

object Customer {
  case object CaffeineWithdrawalWarning
}

class Customer(caffeineSource: ActorRef) extends Actor with ActorLogging {
  import Customer._
  import Barista._
  import EspressoCup._

  context.watch(caffeineSource)

  def receive = {
    case CaffeineWithdrawalWarning => caffeineSource ! EspressoRequest
    case (EspressoCup(Filled), Receipt(amount)) =>
      log.info(s"yay, caffeine for ${self}!")
    case Terminated(barista) =>
      log.info("Oh well, let's find another coffeehouse...")
  }
}