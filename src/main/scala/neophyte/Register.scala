package neophyte

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.AskTimeoutException
import akka.util.Timeout

import scala.concurrent.duration._

object Register {
  sealed trait Article
  case object Espresso extends Article
  case object Cappuccino extends Article
  case class Transaction(article: Article)
  class PaperJamException(msg: String) extends Exception(msg)
  case object ComebackLater
}

class Register extends Actor with ActorLogging {
  import akka.pattern.ask
  import akka.pattern.pipe
  import context.dispatcher
  import Register._
  import Barista._
  import ReceiptPrinter._

  implicit val timeout = Timeout(4.seconds)
  var revenue = 0
  val prices = Map[Article, Int](Espresso -> 150, Cappuccino -> 250)
  val printer = context.actorOf(Props[ReceiptPrinter], "Printer")
  override def postRestart(reason: Throwable) {
    super.postRestart(reason)
    log.info(s"Restarted, and revenue is $revenue cents")
  }
  def receive = {
    case Transaction(article) =>
      val price = prices(article)
      val requester = sender
      (printer ? PrintJob(price)).map((requester, _)).recover {
        case _: AskTimeoutException => {
          log.info("AskTimeoutException. Sending ComebackLater")
          ComebackLater
        }
      }.pipeTo(self)
    case (requester: ActorRef, receipt: Receipt) =>
      revenue += receipt.amount
      log.info(s"revenue is $revenue cents")
      requester ! receipt
  }
}