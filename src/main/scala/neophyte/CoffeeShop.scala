package neophyte

import akka.actor.{ActorSystem, Props}

object CoffeeShop extends App {
  import Customer._
  import Barista._
  val system = ActorSystem("Coffeehouse")
  val barista = system.actorOf(Props[Barista], "Barista")
  val customerJohnny = system.actorOf(Props(classOf[Customer], barista), "Johnny")
  val customerAlina = system.actorOf(Props(classOf[Customer], barista), "Alina")
  customerJohnny ! CaffeineWithdrawalWarning
  customerAlina ! CaffeineWithdrawalWarning

  Thread.sleep(2000)
  barista ! ClosingTime
//  system.stop()
}